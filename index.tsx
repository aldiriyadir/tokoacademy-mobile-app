/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './src/index';


AppRegistry.registerComponent('TokoAcademy', () => App);
