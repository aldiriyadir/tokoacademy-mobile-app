import { StyleSheet, Dimensions } from 'react-native'

let {height, width} = Dimensions.get('window')

const TAAccentBlue = "#0183FC";

export default {
  TAAccentBlue
}

const common_styles = StyleSheet.create({
  
  // 1. LAYOUT 
  // Assets, Receive, Balance, BuySell, EditProfile, Dashboard, forgotPassword, Intro, Login, Market, Rreferal, Register, Withdrawal
  wrapper: {
      flex: 1,
      padding: 10
  },

})