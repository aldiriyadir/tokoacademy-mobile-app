/*
  =================================================
  THIS IS THE APP ENTRY POINT.
  =================================================
*/

import React, { useState, useEffect } from 'react';
import { 
  View, 
  Text, 
  Button,
  ToastAndroid, 
  BackHandler
} from 'react-native';
import EventBus from 'eventing-bus';
import BackgroundJob from 'react-native-background-job'

class TokoAcademy extends React.Component {

  async componentDidMount() {

    EventBus.on('closeLoading', () => {
      this.setState({ showLoading: false })
    });

    EventBus.on('loadingLoader', (message : any) => {
      console.log("loading loader")
      this.setState({
        showLoading: true,
        loadingMessage: message,
        loadingType: 'LOADER',
        loadingAnimationStyle: { height: '150%', width: '150%'},
        loadingAnimationContainerStyle: { height: '75%', width: '75%' },
        loadingContainerStyle: {
          width: 200,
          paddingVertical: 20,
          maxHeight: 130
        }
      })
    })
  }

  handleBackPress = () => {
    ToastAndroid.show('Press once again to close the app', ToastAndroid.SHORT)
    BackHandler.addEventListener('doubleBackPress', this.handleDoubleBackPress)
    setTimeout(() => {
      BackHandler.removeEventListener('doubleBackPress', this.handleDoubleBackPress)
    }, 2000)
    return true
  }

  handleDoubleBackPress = () => {
    BackHandler.exitApp();
    BackgroundJob.cancelAll();
    return true
  }


}

export default TokoAcademy